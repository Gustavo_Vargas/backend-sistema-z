﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SistemaZ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaZ.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ControleController : ControllerBase
    {
        private readonly Context _context;

        public ControleController(Context context)
        {
            _context = context;
        }

        // GET: api/Controle
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Controle>>> GetControle()
        {
            return await _context.Controle.ToListAsync();
        }

        // GET: api/Controle/id
        [HttpGet("{id}")]
        public async Task<ActionResult<Controle>> GetControle(int id)
        {
            var controle = await _context.Controle.FindAsync(id);

            if (controle == null)
            {
                return NotFound();
            }

            return controle;
        }

        // PUT: api/Controle/id
        [HttpPut()]
        public async Task<IActionResult> PutControle([FromBody] Controle controle)
        {
            var controleBanco = await _context.Controle.FindAsync(controle.Id);

            if (controleBanco == null)
                return NotFound();

            controleBanco.Status = controle.Status;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ControleExists(controle.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(200);
        }

        // POST: api/Controle
        [HttpPost]
        public async Task<ActionResult<Controle>> PostControle([FromBody] Controle controle)
        {

            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetControle), new { id = controle.Id }, controle);
        }
        

        private bool ControleExists(string id)
        {
            return _context.Controle.Any(e => e.Id == id);
        }
    }
}