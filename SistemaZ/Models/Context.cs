﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaZ.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options)
             : base(options)
        {
        }
        public DbSet<Controle> Controle { get; set; }
    }
}
