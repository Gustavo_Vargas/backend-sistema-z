﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaZ.Models
{
    public class Controle
    {
        [MaxLength(50)]
        public string Id { get; set; }

        [MaxLength(255)]
        public string Status { get; set; }

        public DateTime Data { get; set; }

    }
}
